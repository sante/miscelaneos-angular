import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-switch',
  templateUrl: './ng-switch.component.html',
  styles: []
})
export class NgSwitchComponent implements OnInit {

  alerta = 0;


  constructor() { }

  ngOnInit() {  }

  cambio () {
    this.alerta = Math.floor((Math.random() * 4) + 1);
  }


}
